class Person {
	constructor(first, last, nick, born, died) {
		this._first = first;
		this._last = last;
		this._nick = nick;
		this._born = born;
		this._died = died;
	}
  
	//setit ja getit pääluokalle
	get first() {
		return this._first;
	}

  set first(newFirst) {
    this._first = newFirst;
  }

	get last() {
		return this._last;
	}
  
  set last(newLast) {
    this._last = newLast;
  }

	get nick() {
		return this._nick;
	}
  
  set nick(newNick) {
    this._nick = newNick;
  }

	get born() {
		return this._born;
	}
  
  set born(newBorn) {
    this._born = newBorn;
  }
  
	get died() {
		return this._died;
	}
  
  set died(newDied) {
    this._died = newDied;
  }
}

class Profession extends Person {
	constructor(first, last, nick, born, died, pictureLink, jobStart, jobEnd, historyLink) {
		super(first, last, nick, born, died);
		this._pictureLink = pictureLink;
		this._jobStart = jobStart;
		this._jobEnd = jobEnd;
		this._historyLink = historyLink;
	}
  
	//setit ja getit luokalle persoona
	get pictureLink() {
		return this._pictureLink;
	}
  
  set pictureLink(newpictureLink) {
    this._pictureLink = newpictureLink;
  }

	get jobStart() {
		return this._jobStart;
	}
  
  set jobStart(newjobStart) {
    this._jobStart = newjobStart;
  }

	get jobEnd() {
		return this._jobEnd;
	}
  
  set jobEnd(newjobEnd) {
    this._jobEnd = newjobEnd;
  }

	get historyLink() {
		return this._historyLink;
	}
  
  set historyLink(newhistoryLink) {
    this._historyLink = newhistoryLink;
  }
}

//luodaan entryjä
let mAhtisaari = new Profession(['Martti'], 'Pallomies', 'Mara', 1937, null, 'http://www.gstatic.com/tv/thumb/persons/847004/847004_v9_ba.jpg', 1994, 2000, 'https://en.wikipedia.org/wiki/Martti_Ahtisaari');
let tHalonen = new Profession(['Tarja','Kaarina'], 'Halonen', 'Muumimamma', 1943, null, 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Tarja_Halonen_1c389_8827-2.jpg/440px-Tarja_Halonen_1c389_8827-2.jpg', 2000, 2012, 'https://en.wikipedia.org/wiki/Tarja_Halonen');

//testausta
document.write(mAhtisaari.first+' '+mAhtisaari.last+"<br>");
mAhtisaari.last = 'Ahtisaari'
document.write(mAhtisaari.first+' '+mAhtisaari.last+"<br>");
let mara = JSON.stringify(mAhtisaari, null, 4);
alert(mara);
let halo = JSON.stringify(tHalonen, null, 4);
alert(halo);

